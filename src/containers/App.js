import React from 'react'
import { connect } from 'react-redux'

import { setField } from '../actions'
import Calculator from '../components/Calculator'

const tenureOptions = [5, 10, 15, 20, 25]

const App = ({ calculatorFields, setField, rates }) => {
  return (
    <Calculator
      {...calculatorFields}
      tenureOptions={tenureOptions}
      setField={setField}
      rates={rates}
    />
  )
}

const mapStateToProps = state => ({
  calculatorFields: state.calculatorFields,
  rates: state.rates
})

const mapDispatchToProps = dispatch => ({
  setField: field => dispatch(setField(field))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)

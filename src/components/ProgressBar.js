import React from 'react'
import styled from 'styled-components'

const ProgressBar = ({ progress }) => (
  <Bar><Fill progress={progress} /></Bar>
)

const Fill = styled.div`
  background: #42A5F5;
  position: absolute;
  left: 0;
  top: 0;
  border-radius: 4px;
  bottom: 0;
  width: ${({ progress }) => progress + '%'};
`

const Bar = styled.div`
  height: 16px;
  border-radius: 4px;
  width: 100%;
  background: #BBDEFB;
  position: relative;
`

export default ProgressBar
